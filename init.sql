CREATE DATABASE IF NOT EXISTS app_db;
USE app_db;

CREATE TABLE IF NOT EXISTS Credentials (
     Email varchar(255) NOT NULL PRIMARY KEY,
     Password varchar(255),
     GoogleToken varchar(255),
     Cookie varchar(255)
);

CREATE TABLE IF NOT EXISTS Profiles (
     Email varchar(255) NOT NULL PRIMARY KEY,
     FullName varchar(255),
     Address varchar(255),
     Telephone varchar(255)
);
