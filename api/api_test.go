package api

import (
	"bitbucket.org/bogdanguranda/bo_gu/db"
	db_mocks "bitbucket.org/bogdanguranda/bo_gu/db/mocks"
	"bitbucket.org/bogdanguranda/bo_gu/ui"
	"bytes"
	"encoding/json"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func setupMocks(t *testing.T) (*gomock.Controller, *db_mocks.MockDB) {
	mockCtrl := gomock.NewController(t)
	mockDB := db_mocks.NewMockDB(mockCtrl)
	return mockCtrl, mockDB
}

func mapSuccessResp(resp *http.Response) (*SuccessResp, error) {
	respBodyBytes, _ := ioutil.ReadAll(resp.Body)
	successResp := SuccessResp{}
	return &successResp, json.Unmarshal(respBodyBytes, &successResp)
}

func mapProfileResp(resp *http.Response) (*db.UserProfile, error) {
	respBodyBytes, _ := ioutil.ReadAll(resp.Body)
	profileResp := db.UserProfile{}
	return &profileResp, json.Unmarshal(respBodyBytes, &profileResp)
}

func mapError(resp *http.Response) (*JSONErr, error) {
	respBodyBytes, _ := ioutil.ReadAll(resp.Body)
	err := JSONErr{}
	return &err, json.Unmarshal(respBodyBytes, &err)
}

func TestRegister_ValidPayload_Return200(t *testing.T) {
	mockCtrl, mockDB := setupMocks(t)
	defer mockCtrl.Finish()
	defAPI := NewDefaultAPI(mockDB)

	mockDB.EXPECT().UpdateCookie("something@gmail.com", gomock.Any()).Return(nil)

	regReq := db.UserCredentials{
		Email: "something@gmail.com",
		Password: "some-pass",
	}
	reqBody, _ := json.Marshal(regReq)
	req := httptest.NewRequest("POST", ui.APIRegisterPath, bytes.NewReader(reqBody))
	w := httptest.NewRecorder()
	defAPI.Register(w, req)

	resp, err := mapSuccessResp(w.Result())
	assert.Nil(t, err)

	assert.Equal(t, http.StatusOK, w.Result().StatusCode)
	assert.NotEmpty(t, resp.Cookie)
}

func TestRegister_InvalidPayloadFormat_Return400(t *testing.T) {
	mockCtrl, mockDB := setupMocks(t)
	defer mockCtrl.Finish()
	defAPI := NewDefaultAPI(mockDB)

	reqBody := "this is not a json payload"
	req := httptest.NewRequest("POST", ui.APIRegisterPath, bytes.NewReader([]byte(reqBody)))
	w := httptest.NewRecorder()
	defAPI.Register(w, req)

	resp, err := mapError(w.Result())
	assert.Nil(t, err)

	assert.Equal(t, http.StatusOK, w.Result().StatusCode)
	assert.NotNil(t, resp)
}

func TestRegister_MissingEmail_Return400(t *testing.T) {

}

func TestRegister_MissingPassword_Return400(t *testing.T) {

}

func TestLogIn_ValidPayload_Return200(t *testing.T) {
	mockCtrl, mockDB := setupMocks(t)
	defer mockCtrl.Finish()
	defAPI := NewDefaultAPI(mockDB)

	creds := &db.UserCredentials{
		Email: "something@gmail.com",
		Password: "some-pass",
	}

	mockDB.EXPECT().GetUserCredentials("something@gmail.com").Return(creds, nil)
	mockDB.EXPECT().UpdateCookie("something@gmail.com", gomock.Any()).Return(nil)

	regReq := db.UserCredentials{
		Email: "something@gmail.com",
		Password: "some-pass",
	}
	reqBody, _ := json.Marshal(regReq)
	req := httptest.NewRequest("POST", ui.APILoginPath, bytes.NewReader(reqBody))
	w := httptest.NewRecorder()
	defAPI.LogIn(w, req)

	resp, err := mapSuccessResp(w.Result())
	assert.Nil(t, err)

	assert.Equal(t, http.StatusOK, w.Result().StatusCode)
	assert.NotEmpty(t, resp.Cookie)
}

func TestLogIn_InvalidPayloadFormat_Return400(t *testing.T) {
	mockCtrl, mockDB := setupMocks(t)
	defer mockCtrl.Finish()
	defAPI := NewDefaultAPI(mockDB)

	reqBody := "this is not a json payload"
	req := httptest.NewRequest("POST", ui.APILoginPath, bytes.NewReader([]byte(reqBody)))
	w := httptest.NewRecorder()
	defAPI.LogIn(w, req)

	resp, err := mapError(w.Result())
	assert.Nil(t, err)

	assert.Equal(t, http.StatusOK, w.Result().StatusCode)
	assert.NotNil(t, resp)
}

func TestLogIn_WrongPassword_Return400(t *testing.T) {

}

func TestLogOut_ValidPayload_Return200(t *testing.T) {
	mockCtrl, mockDB := setupMocks(t)
	defer mockCtrl.Finish()
	defAPI := NewDefaultAPI(mockDB)

	mockDB.EXPECT().FindCookie(gomock.Any()).Return(nil)
	mockDB.EXPECT().UpdateCookie("something@gmail.com", "").Return(nil)

	regReq := db.UserCredentials{
		Email: "something@gmail.com",
		Password: "some-pass",
	}
	reqBody, _ := json.Marshal(regReq)
	req := httptest.NewRequest("POST", ui.APILogoutPath, bytes.NewReader(reqBody))
	w := httptest.NewRecorder()
	defAPI.LogOut(w, req)

	assert.Equal(t, http.StatusOK, w.Result().StatusCode)
}

func TestLogOut_InvalidPayloadFormat_Return400(t *testing.T) {
	mockCtrl, mockDB := setupMocks(t)
	defer mockCtrl.Finish()
	defAPI := NewDefaultAPI(mockDB)

	reqBody := "this is not a json payload"
	req := httptest.NewRequest("POST", ui.APILogoutPath, bytes.NewReader([]byte(reqBody)))
	w := httptest.NewRecorder()
	defAPI.LogOut(w, req)

	resp, err := mapError(w.Result())
	assert.Nil(t, err)

	assert.Equal(t, http.StatusOK, w.Result().StatusCode)
	assert.NotNil(t, resp)
}

func TestLogOut_WrongCookie_Return401(t *testing.T) {

}

func TestGetProfile_ValidPayload_Return200(t *testing.T) {

}

func TestGetProfile_InvalidPayloadFormat_Return400(t *testing.T) {
	mockCtrl, mockDB := setupMocks(t)
	defer mockCtrl.Finish()
	defAPI := NewDefaultAPI(mockDB)

	reqBody := "this is not a json payload"
	req := httptest.NewRequest("POST", ui.APIGetProfilePath, bytes.NewReader([]byte(reqBody)))
	w := httptest.NewRecorder()
	defAPI.GetProfile(w, req)

	resp, err := mapError(w.Result())
	assert.Nil(t, err)

	assert.Equal(t, http.StatusOK, w.Result().StatusCode)
	assert.NotNil(t, resp)
}

func TestGetProfile_WrongCookie_Return401(t *testing.T) {

}

func TestUpdateProfile_ValidPayload_Return200(t *testing.T) {

}

func TestUpdateProfile_InvalidPayloadFormat_Return400(t *testing.T) {
	mockCtrl, mockDB := setupMocks(t)
	defer mockCtrl.Finish()
	defAPI := NewDefaultAPI(mockDB)

	reqBody := "this is not a json payload"
	req := httptest.NewRequest("POST", ui.APIUpdateProfilePath, bytes.NewReader([]byte(reqBody)))
	w := httptest.NewRecorder()
	defAPI.UpdateProfile(w, req)

	resp, err := mapError(w.Result())
	assert.Nil(t, err)

	assert.Equal(t, http.StatusOK, w.Result().StatusCode)
	assert.NotNil(t, resp)
}

func TestUpdateProfile_WrongCookie_Return401(t *testing.T) {

}