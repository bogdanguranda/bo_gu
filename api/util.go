package api

import (
	"bitbucket.org/bogdanguranda/bo_gu/db"
	"encoding/json"
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
)

type JSONErr struct {
	Error string `json:"error"`
}

type SuccessResp struct {
	Cookie string `json:"cookie,omitempty"`
	ResetLink string `json:"reset_link,omitempty"`
}

// MapCredentialsPayload maps a http request to user credentials model.
func (dAPI *DefaultAPI) mapCredentialsPayload(r *http.Request) (*db.UserCredentials, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	creds := db.UserCredentials{}
	return &creds, json.Unmarshal(body, &creds)
}

// MapProfilePayload maps a http request to user profile model.
func (dAPI *DefaultAPI) mapProfilePayload(r *http.Request) (*db.UserProfile, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	profile := db.UserProfile{}
	return &profile, json.Unmarshal(body, &profile)
}

func (dAPI *DefaultAPI) createNewCookie(email string) (string, error) {
	cookie, err := dAPI.cookieHandler.Encode("cookie", map[string]string{"email": email})
	if err != nil {
		return "", err
	}

	if err := dAPI.db.UpdateCookie(email, cookie); err != nil {
		return "", err
	}

	return cookie, nil
}

func (dAPI *DefaultAPI) clearCookie(email string) error {
	return dAPI.db.UpdateCookie(email, "")
}

func (dAPI *DefaultAPI) authenticate(creds *db.UserCredentials) error {
	existingCreds, err := dAPI.db.GetUserCredentials(creds.Email)
	if err != nil {
		return err
	}

	if existingCreds.Password != creds.Password || existingCreds.GoogleToken != creds.GoogleToken {
		return errors.New("Authentication failed")
	}

	return nil
}