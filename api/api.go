package api

import (
	"bitbucket.org/bogdanguranda/bo_gu/db"
	"encoding/json"
	"github.com/gorilla/securecookie"
	"net/http"
)

// API interface for the app's REST API.
type API interface {
	Register(w http.ResponseWriter, r *http.Request)
	LogIn(w http.ResponseWriter, r *http.Request)
	LogOut(w http.ResponseWriter, r *http.Request)
	GetProfile(w http.ResponseWriter, r *http.Request)
	UpdateProfile(w http.ResponseWriter, r *http.Request)
	ResetPasswordLink(w http.ResponseWriter, r *http.Request)
	ResetPassword(w http.ResponseWriter, r *http.Request)
}

// DefaultAPI default implementation for API.
type DefaultAPI struct {
	db   db.DB
	cookieHandler *securecookie.SecureCookie
}

// NewDefaultAPI creates a new DefaultAPI.
func NewDefaultAPI(db db.DB) *DefaultAPI {
	return &DefaultAPI{db: db, cookieHandler: securecookie.New(securecookie.GenerateRandomKey(64), securecookie.GenerateRandomKey(32))}
}

// Register handles a new user registration.
func (dAPI *DefaultAPI) Register(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	creds, err := dAPI.mapCredentialsPayload(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp, _ := json.Marshal(JSONErr{Error: "Invalid payload."})
		w.Write(resp)
		return
	}

	cookie, err := dAPI.createNewCookie(creds.Email)
	if  err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp, _ := json.Marshal(JSONErr{Error: "There was a problem with the server."})
		w.Write(resp)
		return
	}

	w.WriteHeader(http.StatusOK)
	resp, _ := json.Marshal(SuccessResp{Cookie: cookie})
	w.Write(resp)
}

func (dAPI *DefaultAPI) LogIn(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	creds, err := dAPI.mapCredentialsPayload(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp, _ := json.Marshal(JSONErr{Error: "Invalid payload."})
		w.Write(resp)
		return
	}

	if err := dAPI.authenticate(creds); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp, _ := json.Marshal(JSONErr{Error: err.Error()})
		w.Write(resp)
		return
	}

	cookie, err := dAPI.createNewCookie(creds.Email)
	if  err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp, _ := json.Marshal(JSONErr{Error: "There was a problem with the server."})
		w.Write(resp)
		return
	}

	w.WriteHeader(http.StatusOK)
	resp, _ := json.Marshal(SuccessResp{Cookie: cookie})
	w.Write(resp)
}

func (dAPI *DefaultAPI) LogOut(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	creds, err := dAPI.mapCredentialsPayload(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp, _ := json.Marshal(JSONErr{Error: "Invalid payload."})
		w.Write(resp)
		return
	}

	if err := dAPI.db.FindCookie(creds.Cookie); err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		resp, _ := json.Marshal(JSONErr{Error: "Not authorized to see this page."})
		w.Write(resp)
		return
	}

	if  err := dAPI.clearCookie(creds.Email); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp, _ := json.Marshal(JSONErr{Error: "There was a problem with the server."})
		w.Write(resp)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (dAPI *DefaultAPI) GetProfile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	creds, err := dAPI.mapCredentialsPayload(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp, _ := json.Marshal(JSONErr{Error: "Invalid payload."})
		w.Write(resp)
		return
	}

	if err := dAPI.db.FindCookie(creds.Cookie); err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		resp, _ := json.Marshal(JSONErr{Error: "Not authorized to see this page."})
		w.Write(resp)
		return
	}

	profile, err := dAPI.db.GetUserProfile(creds.Email)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp, _ := json.Marshal(JSONErr{Error: "Not authorized to see this page."})
		w.Write(resp)
		return
	}

	w.WriteHeader(http.StatusOK)
	resp, _ := json.Marshal(profile)
	w.Write(resp)
}

func (dAPI *DefaultAPI) UpdateProfile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	profile, err := dAPI.mapProfilePayload(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp, _ := json.Marshal(JSONErr{Error: "Invalid payload."})
		w.Write(resp)
		return
	}

	if err := dAPI.db.FindCookie(profile.Cookie); err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		resp, _ := json.Marshal(JSONErr{Error: "Not authorized to see this page."})
		w.Write(resp)
		return
	}

	oldProfile, err := dAPI.db.GetUserProfileByCookie(profile.Cookie)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp, _ := json.Marshal(JSONErr{Error: "Not authorized to see this page."})
		w.Write(resp)
		return
	}

	if err := dAPI.db.UpdateUserProfile(oldProfile.Email, profile); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp, _ := json.Marshal(JSONErr{Error: "Not authorized to see this page."})
		w.Write(resp)
		return
	}

	w.WriteHeader(http.StatusOK)
	resp, _ := json.Marshal(profile)
	w.Write(resp)
}

func (dAPI *DefaultAPI) ResetPasswordLink(w http.ResponseWriter, r *http.Request) {
	//w.Header().Set("Content-Type", "application/json")
	//
	//creds, err := dAPI.mapCredentialsPayload(r)
	//if err != nil {
	//	w.WriteHeader(http.StatusBadRequest)
	//	resp, _ := json.Marshal(JSONErr{Error: "Invalid payload."})
	//	w.Write(resp)
	//	return
	//}
	//
	//cookie, err := dAPI.createNewCookie(creds.Email)
	//if  err != nil {
	//	w.WriteHeader(http.StatusInternalServerError)
	//	resp, _ := json.Marshal(JSONErr{Error: "There was a problem with the server."})
	//	w.Write(resp)
	//	return
	//}
	//
	//w.WriteHeader(http.StatusOK)
	//resp, _ := json.Marshal(SuccessResp{ResetLink: "http://heroku.com/something/" + cookie})
	//w.Write(resp)
	w.WriteHeader(http.StatusNotImplemented)
}

func (dAPI *DefaultAPI) ResetPassword(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotImplemented)
}
