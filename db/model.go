package db

type UserCredentials struct {
	Email string    `json:"email" binding:"required"`
	Password string `json:"password,omitempty"`
	GoogleToken string `json:"google_token,omitempty"`
	Cookie string `json:"cookie,omitempty"`
}

// UserProfile contains user profile data.
type UserProfile struct {
	Email string    `json:"email" binding:"required"`
	FullName   string `json:"full_name,omitempty"`
	Address string `json:"address,omitempty"`
	Telephone string `json:"telephone,omitempty"`

	Cookie string `json:"cookie,omitempty"` // this is just for requests auth, it's not saved in DB and not exposed on response
}
