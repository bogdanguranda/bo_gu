package db

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/sirupsen/logrus"
	"time"
)

//go:generate mockgen -destination=./mocks/mock_db.go -package=db bitbucket.org/bogdanguranda/bo_gu/db DB

// DB describes CRUD methods for a database containing orders.
type DB interface {
	InsertUserCredentials(creds *UserCredentials) error
	GetUserCredentials(email string) (*UserCredentials, error)
	GetUserProfile(email string) (*UserProfile, error)
	GetUserProfileByCookie(cookie string) (*UserProfile, error)
	UpdateUserProfile(email string, profile *UserProfile) error
	UpdateCookie(email, cookie string) error
	FindCookie(cookie string) error
}

// MySqlDB implements DB interface using MySQL as database.
type MySqlDB struct {
	db *sql.DB
}

// NewMySqlDB creates a new MySqlDB.
func NewMySqlDB(mySqlURL string) (*MySqlDB, error) {
	db, err := TryConnect(mySqlURL, 3, 5)
	if err != nil {
		return nil, err
	}

	return &MySqlDB{db: db}, nil
}

// TryConnect retry system for connecting to MySQL database.
func TryConnect(dsn string, delay, retries int) (*sql.DB, error) {
	db, err := sql.Open("mysql", dsn)
	for ; err != nil && retries > 0; retries-- {
		time.Sleep(time.Second * time.Duration(delay))
		db, err = sql.Open("mysql", dsn)
	}
	return db, err
}

// Close closes the database connection.
func (my *MySqlDB) Close() {
	if err := my.db.Close(); err != nil {
		logrus.Panic("Failed to close MySQL db.")
	}
}

// InsertUserCredentials .
func (my *MySqlDB) InsertUserCredentials(creds *UserCredentials) error {
	insertQuery, err := my.db.Prepare("INSERT INTO Credentials VALUES(?, ?, ?);")
	if err != nil {
		return err
	}

	if _, err := insertQuery.Exec(creds.Email, creds.Password, creds.GoogleToken); err != nil {
		return err
	}

	return nil
}

// UpdateUserProfile .
func (my *MySqlDB) UpdateUserProfile(email string, profile *UserProfile) error {
	updateQuery, err := my.db.Prepare("UPDATE Profiles SET Email = ?, FullName = ?, Address = ?, Telephone = ? WHERE Email = ?;")
	if err != nil {
		return err
	}

	if _, err = updateQuery.Exec(profile.Email, profile.FullName, profile.Address, profile.Telephone, email); err != nil {
		return err
	}

	return nil
}

// GetUserCredentials .
func (my *MySqlDB) GetUserCredentials(email string) (*UserCredentials, error) {
	row := my.db.QueryRow("SELECT * FROM Credentials WHERE Email = " + email + ";")

	creds := &UserCredentials{}
	if err := row.Scan(&creds.Email, &creds.Password, &creds.GoogleToken); err != nil {
		return nil, err
	}

	return creds, nil
}

// GetUserProfile .
func (my *MySqlDB) GetUserProfile(email string) (*UserProfile, error) {
	row := my.db.QueryRow("SELECT * FROM Profiles WHERE Email = " + email + ";")

	profile := &UserProfile{}
	if err := row.Scan(&profile.Email, &profile.FullName, &profile.Address, &profile.Telephone); err != nil {
		return nil, err
	}

	return profile, nil
}

// GetUserProfileByCookie .
func (my *MySqlDB) GetUserProfileByCookie(cookie string) (*UserProfile, error) {
	row := my.db.QueryRow("SELECT p.Email, p.FullName, p.Address, p.Telephone FROM Credentials c JOIN Profiles p ON c.Email = p.Email WHERE c.Cookie = " + cookie + ";")

	profile := &UserProfile{}
	if err := row.Scan(&profile.Email, &profile.FullName, &profile.Address, &profile.Telephone); err != nil {
		return nil, err
	}

	return profile, nil
}

// InsertCookie .
func (my *MySqlDB) UpdateCookie(email, cookie string) error {
	updateQuery, err := my.db.Prepare("UPDATE Credentials SET Cookie = ? WHERE Email = ?;")
	if err != nil {
		return err
	}

	if _, err = updateQuery.Exec(cookie, email); err != nil {
		return err
	}

	return nil
}

// FindCookie .
func (my *MySqlDB) FindCookie(cookie string) error {
	row := my.db.QueryRow("SELECT * FROM Credentials WHERE Cookie = " + cookie + ";")
	var found string
	return row.Scan(&found)
}
