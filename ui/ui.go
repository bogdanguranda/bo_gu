package ui

import (
	"bitbucket.org/bogdanguranda/bo_gu/db"
	"bytes"
	"encoding/json"
	"html/template"
	"net/http"
)

const (
	// These URLs assume both UI and API apps run on the same machine.
	APIRegisterPath = "http://127.0.0.1:8081/register"
	APILoginPath = "http://127.0.0.1:8081/login"
	APILogoutPath = "http://127.0.0.1:8081/logout"
	APIGetProfilePath = "http://127.0.0.1:8081/profile"
	APIUpdateProfilePath = "http://127.0.0.1:8081/profile"
	APIResetPath = "http://127.0.0.1:8081/reset"

	TemplatesPath = "ui/templates/"
)

// API interface for the app's REST API.
type UI interface {
	RootHandler(w http.ResponseWriter, r *http.Request)

	RegisterHandler(w http.ResponseWriter, r *http.Request)

	LoginPageHandler(w http.ResponseWriter, r *http.Request)
	LoginHandler(w http.ResponseWriter, r *http.Request)
	LogoutHandler(w http.ResponseWriter, r *http.Request)

	FillPageHandler(w http.ResponseWriter, r *http.Request)
	FillHandler(w http.ResponseWriter, r *http.Request)

	ProfilePageHandler(w http.ResponseWriter, r *http.Request)
	ProfileHandler(w http.ResponseWriter, r *http.Request)

	ResetHandler(w http.ResponseWriter, r *http.Request)
}

// DefaultUI default implementation for UI.
type DefaultUI struct {
}

// NewDefaultUI creates a new DefaultUI.
func NewDefaultUI() *DefaultUI {
	return &DefaultUI{}
}

// RootHandler redirects to login page.
func (ui *DefaultUI) RootHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/login", 302)
}

// RegisterHandler processes register request.
func (ui *DefaultUI) RegisterHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	apiReqBody, _ := json.Marshal(db.UserCredentials{Email: r.FormValue("email"), Password: r.FormValue("password")})
	resp, err := http.Post(APIRegisterPath, "application/json", bytes.NewBuffer(apiReqBody))
	if  err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	cookie, err := ui.getCookie(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	ui.setCookie(cookie, w)
	http.Redirect(w, r, "/fill", 302)
}

// LoginPageHandler loads login page.
func (ui *DefaultUI) LoginPageHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles(TemplatesPath + "login.gohtml")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := t.Execute(w, nil); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// LoginHandler processes login request.
func (ui *DefaultUI) LoginHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	apiReqBody, _ := json.Marshal(db.UserCredentials{Email: r.FormValue("email"), Password: r.FormValue("password")})
	resp, err := http.Post(APILoginPath, "application/json", bytes.NewBuffer(apiReqBody))
	if  err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	cookie, err := ui.getCookie(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	ui.setCookie(cookie, w)
	http.Redirect(w, r, "/profile", 302)
}

// ResetHandler reset password processing.
func (ui *DefaultUI) ResetHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	apiReqBody, _ := json.Marshal(db.UserCredentials{Email: r.FormValue("email")})
	resp, err := http.Post(APIResetPath, "application/json", bytes.NewBuffer(apiReqBody))
	if  err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		http.Error(w, "Failed to send reset email", resp.StatusCode)
		return
	}

	t, err := template.ParseFiles(TemplatesPath + "login.gohtml")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := t.Execute(w, RespMessage{Message: "Reset link was sent successfully!"}); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// FillPageHandler loads finish registration page.
func (ui *DefaultUI) FillPageHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles(TemplatesPath + "fill.gohtml")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := t.Execute(w, nil); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// FillHandler processes finish registration request.
func (ui *DefaultUI) FillHandler(w http.ResponseWriter, r *http.Request) {
	_, statusCode, err := ui.updateProfile(r)
	if err != nil {
		http.Error(w, err.Error(), statusCode)
		return
	}

	http.Redirect(w, r, "/profile", 302)
}

// ProfilePageHandler loads profile page.
func (ui *DefaultUI) ProfilePageHandler(w http.ResponseWriter, r *http.Request) {
	profile, statusCode, err := ui.getProfileWithCookie(r)
	if err != nil {
		http.Error(w, err.Error(), statusCode)
		return
	}

	t, err := template.ParseFiles(TemplatesPath + "profile.gohtml")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := t.Execute(w, profile); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// ProfileHandler processes profile update request.
func (ui *DefaultUI) ProfileHandler(w http.ResponseWriter, r *http.Request) {
	profile, statusCode, err := ui.updateProfile(r)
	if err != nil {
		http.Error(w, err.Error(), statusCode)
	}

	t, err := template.ParseFiles(TemplatesPath + "profile.gohtml")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := t.Execute(w, profile); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// LogoutHandler .
func (ui *DefaultUI) LogoutHandler(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("cookie")
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	creds := db.UserCredentials{Cookie: cookie.Value}
	apiReqBody, _ := json.Marshal(creds)
	resp, err := http.Post(APILogoutPath, "application/json", bytes.NewBuffer(apiReqBody))
	if  err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		http.Error(w, "Failed to logout", http.StatusInternalServerError)
		return
	}

	ui.clearCookie(w)
	http.Redirect(w, r, "/login", 302)
}