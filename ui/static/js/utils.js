function toggleForm(formToShow) {
    hideAllForms();
    document.getElementById(formToShow).style.display = "block";
}

function hideAllForms() {
    document.getElementById("register-form").style.display = "none";
    document.getElementById("login-form").style.display = "none";
    document.getElementById("reset-form").style.display = "none";
}

function toggleProfileEdit() {
    var profileFieldset = document.getElementById("profile-fieldset");
    var editButton = document.getElementById("edit-button");
    if (profileFieldset.disabled === "disabled") {
        profileFieldset.disabled = "";
        editButton.innerText = "Cancel";
    } else {
        profileFieldset.disabled = "disabled";
        editButton.innerText = "Edit";
    }
}