package ui

import (
	"bitbucket.org/bogdanguranda/bo_gu/api"
	"bitbucket.org/bogdanguranda/bo_gu/db"
	"bytes"
	"encoding/json"
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
)

type RespMessage struct {
	Message string
}

func (ui *DefaultUI) getCookie(resp *http.Response) (string, error) {
	body, _ := ioutil.ReadAll(resp.Body)
	success := api.SuccessResp{}
	if err := json.Unmarshal(body, &success); err != nil {
		return "", err
	}
	return success.Cookie, nil
}

// SetCookie set session cookie.
func (ui *DefaultUI) setCookie(cookieEncoded string, response http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:  "cookie",
		Value: cookieEncoded,
		Path:  "/",
	}
	http.SetCookie(response, cookie)
}

// ClearCookie clear session cookie.
func (ui *DefaultUI) clearCookie(response http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:   "cookie",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}
	http.SetCookie(response, cookie)
}

func (ui *DefaultUI) getProfileWithCookie(r *http.Request) (*db.UserProfile, int, error) {
	cookie, err := r.Cookie("cookie")
	if err != nil {
		return nil, http.StatusUnauthorized, err
	}

	req, err := http.NewRequest(http.MethodGet, APIGetProfilePath, nil)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	req.AddCookie(cookie)

	resp, err := http.DefaultClient.Do(req)
	if  err != nil {
		return nil, http.StatusInternalServerError, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, resp.StatusCode, errors.New("Failed to access")
	}

	body, _ := ioutil.ReadAll(resp.Body)
	profile := db.UserProfile{}
	if err := json.Unmarshal(body, &profile); err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return &profile, http.StatusOK, nil
}

func (ui *DefaultUI) updateProfile(r *http.Request) (*db.UserProfile, int, error) {
	cookie, err := r.Cookie("cookie")
	if err != nil {
		return nil, http.StatusUnauthorized, err
	}

	profile := db.UserProfile{Email: r.FormValue("email"), FullName: r.FormValue("fullName"), Address: r.FormValue("address"),
		Telephone: r.FormValue("telephone"), Cookie: cookie.Value}
	apiReqBody, _ := json.Marshal(profile)
	resp, err := http.Post(APIUpdateProfilePath, "application/json", bytes.NewBuffer(apiReqBody))
	if  err != nil {
		return nil, http.StatusInternalServerError, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, resp.StatusCode, errors.New("Failed to update profile")
	}

	profile.Cookie = ""
	return &profile, http.StatusOK, nil
}