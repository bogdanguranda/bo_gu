module bitbucket.org/bogdanguranda/bo_gu

go 1.12

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/golang/mock v1.3.1 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/securecookie v1.1.1
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.2.2
	google.golang.org/appengine v1.6.1 // indirect
)
