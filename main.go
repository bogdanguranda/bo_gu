package main

import (
	"bitbucket.org/bogdanguranda/bo_gu/api"
	"bitbucket.org/bogdanguranda/bo_gu/db"
	"bitbucket.org/bogdanguranda/bo_gu/ui"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"log"
	"net/http"
	"os"
)

const (
	portUI    = "8080"
	portAPI   = "8081"
	staticDir = "/ui/static/"
)

func main() {
	mySqlURL := os.Getenv("MYSQL_URL")
	if mySqlURL == "" {
		log.Fatal("Env var MYSQL_URL  was not set!")
	}

	uiPort := os.Getenv("PORT")
	if uiPort == "" {
		uiPort = portUI
	}

	defer func() { // recover panics so that an API crash won't affect UI and vice versa
		if err := recover(); err != nil {
			logrus.Errorf("App crashed! Error was %v", err)
		}
	}()

	go startAPIServer(mySqlURL)
	startUIServer(uiPort)
}

func startAPIServer(mySqlURL string) {
	dbMySQL, err := db.NewMySqlDB(mySqlURL)
	if err != nil {
		logrus.Fatal(errors.Wrapf(err, "failed to start MySQL"))
	}
	defer dbMySQL.Close()

	appAPI := api.NewDefaultAPI(dbMySQL)

	router := mux.NewRouter()
	router.HandleFunc("/register", appAPI.Register).Methods(http.MethodPost)
	router.HandleFunc("/login", appAPI.LogIn).Methods(http.MethodPost)
	router.HandleFunc("/logout", appAPI.LogOut).Methods(http.MethodPost)
	router.HandleFunc("/profile", appAPI.GetProfile).Methods(http.MethodGet)
	router.HandleFunc("/profile", appAPI.UpdateProfile).Methods(http.MethodPost)
	router.HandleFunc("/reset", appAPI.ResetPassword).Methods(http.MethodPost)

	logrus.Info("REST API server listening on port " + portAPI)
	if err := http.ListenAndServe(":"+portAPI, router); err != nil {
		log.Fatal("Failed to listen and serve on port " + portAPI)
	}
}

func startUIServer(uiPort string) {
	uiApp := ui.NewDefaultUI()

	router := mux.NewRouter()
	router.HandleFunc("/", uiApp.RootHandler).Methods(http.MethodGet)

	router.HandleFunc("/register", uiApp.RegisterHandler).Methods(http.MethodPost)

	router.HandleFunc("/login", uiApp.LoginPageHandler).Methods(http.MethodGet)
	router.HandleFunc("/login", uiApp.LoginHandler).Methods(http.MethodPost)

	router.HandleFunc("/reset", uiApp.ResetHandler).Methods(http.MethodPost)

	router.HandleFunc("/logout", uiApp.LogoutHandler).Methods(http.MethodPost)

	router.HandleFunc("/fill", uiApp.FillPageHandler).Methods(http.MethodGet)
	router.HandleFunc("/fill", uiApp.FillHandler).Methods(http.MethodPost)

	router.HandleFunc("/profile", uiApp.ProfilePageHandler).Methods(http.MethodGet)
	router.HandleFunc("/profile", uiApp.ProfileHandler).Methods(http.MethodPost)

	router.PathPrefix(staticDir).Handler(http.StripPrefix(staticDir, http.FileServer(http.Dir("."+staticDir))))

	logrus.Info("UI server listening on port " + uiPort)
	if err := http.ListenAndServe(":"+portUI, router); err != nil {
		log.Fatal("Failed to listen and serve on port " + portUI)
	}
}
