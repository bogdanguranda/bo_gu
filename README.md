# BO_GU

A simple Golang login and profile page with frontend in Go Templates and backend in Go REST API.

## Installation

After cloning the repo, assuming you have `docker` installed on your machine, do the following steps in the project folder:

1. For local testing with MySQL run by Docker set MYSQL env var `MYSQL_PASS` and `CLEARDB_DATABASE_URL`as the MySQL URL. 
2. Run `docker-compose up` or `docker-compose up --build` if you want to rebuild everything and ignore the cache. 

## Tests

Steps:
1. Generate mocks with `go generate ./...`
2. To run all tests, including integration with Google Maps API, you need to set `GOOGLE_MAPS_KEY` environment variable
3. Run all tests with: `go test -v ./...`